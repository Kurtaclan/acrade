using System;
using UnityEngine;

namespace UnityStandardAssets.Effects
{
    public class ParticleSystemMultiplier : MonoBehaviour
    {
        // a simple script to scale the size, speed and lifetime of a particle system

        public float multiplier = 1;


        private void Start()
        {
            var systems = GetComponentsInChildren<ParticleSystem>();
            foreach (ParticleSystem system in systems)
            {
                ParticleSystem.MainModule main = system.main;

                ParticleSystem.MinMaxCurve sizeCurve = main.startSize;
                sizeCurve.constant *= multiplier;
                main.startSize = sizeCurve;
                ParticleSystem.MinMaxCurve startCurve = main.startSpeed;
                startCurve.constant *= multiplier;
                main.startSpeed = startCurve;
                ParticleSystem.MinMaxCurve lifeCurve = main.startLifetime;
                lifeCurve.constant *= Mathf.Lerp(multiplier, 1, 0.5f);
                main.startLifetime = lifeCurve;

                system.Clear();
                system.Play();
            }
        }
    }
}
