﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ButtonEvents : MonoBehaviour
{

    public void Quit()
    {
        Application.Quit();
    }

    public void LoadSpaceInvaders()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Scenes/SpaceInvaders");
    }

    public void LoadQBert()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Scenes/QBert");
    }

    public void LoadBustAMove()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Scenes/BustAMove");
    }

    public void LoadVerticalShooter()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Scenes/VerticalShooter");
    }

    public void LoadPacMan()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Scenes/PacMan");
    }

    public void QuitGame()
    {
        SceneManager.LoadScene("Scenes/Main Menu");
    }
}
