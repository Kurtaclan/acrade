﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Pac_Man
{
    public class PacUnit : MonoBehaviour
    {
        public PacGameController gameController;
        public Rigidbody2D rigidBody2d;
        public Animator animator;
        public AudioSource audioSource;
        void Start()
        {
            rigidBody2d = this.GetComponent<Rigidbody2D>();
            animator = this.GetComponent<Animator>();
            audioSource = this.GetComponent<AudioSource>();

            gameController = FindObjectOfType<PacGameController>();

            StartFunctions();
        }

        public virtual void StartFunctions() { }

        public IEnumerator WaitForAudio(Delegate method, params object[] args)
        {
            gameController.PauseGame();

            while (audioSource.isPlaying)
            {
                yield return new WaitForSeconds(0.1f);
            }

            method.DynamicInvoke(args);
            gameController.UnPauseGame();
        }
    }
}
