﻿using UnityEngine;

namespace Assets.Scripts.Pac_Man
{
    public struct NavTile
    {
        public float X;
        public float Y;
        public bool IsNav;

        public NavTile(float x, float y, bool isNav = true)
        {
            X = x;
            Y = y;   
            IsNav = isNav;
        }

        public bool IsZero { get { return (X == 0 && Y == 0); } }
        public Vector3 toVector3() { return new Vector3(X, Y, 1); }
    }

    public struct Tile
    {
        public int X;
        public int Y;
       
        public Tile(int x, int y)
        {
            X = x;
            Y = y;
        }

        public bool IsZero { get { return (X == 0 && Y == 0); } }

        public Tile Add(Tile tile)
        {
            Tile newTile = new Tile();
            newTile.X = X + tile.X;
            newTile.Y = Y + tile.Y;            
            return newTile;
        }
    }
}