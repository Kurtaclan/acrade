﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Pac_Man
{
    public class ScorePanel : MonoBehaviour
    {
        private int lives = 3;
        private int score;
        public int ScoreThreshold;
        public PacGameController gameController;
        public Text ScoreText;
        public Text LivesText;

        void Start()
        {
            ScoreText.text = "Score " + score.ToString("D16");
        }

        public void ChangeScore(int scoreChange)
        {
            score += scoreChange;
            ScoreText.text = "Score " + score.ToString("D12");

            if (score % ScoreThreshold == 0)
            {
                ChangeLives(1);
            }
        }

        public void ChangeLives(int liveChange)
        {
            lives += liveChange;
            LivesText.text = "x" + lives.ToString();

            if (liveChange < 0)
            {
                if (lives == 0)
                {
                    gameController.GameOver();
                }
                else
                {
                    gameController.RestartLevel();
                }
            }
        }
    }
}
