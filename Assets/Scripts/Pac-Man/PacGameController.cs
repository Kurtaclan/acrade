﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Pathfinding;
using System.Collections;

namespace Assets.Scripts.Pac_Man
{
    public class PacGameController : MonoBehaviour
    {
        public int ghostsSpawned = 1;
        public PausePanel pausePanel;
        public GameObject CollectibleContainer;
        private PacManPlayerController pacMan;
        private PacManPlayerController PacMan
        {
            get
            {
                if (pacMan == null)
                {
                    pacMan = FindObjectOfType<PacManPlayerController>();
                }

                return pacMan;
            }
        }
        private List<GhostAi> Ghosts
        {
            get
            {
                if (ghosts == null)
                {
                    ghosts = FindObjectsOfType<GhostAi>().OrderBy(x => x.ghost).ToList();
                }

                return ghosts;
            }
        }
        private List<GhostAi> ghosts;
        public Transform[] SpawnPoints;
        public ScorePanel ScorePanel;
        public bool Pause
        {
            get { return pause; }
            internal set
            {
                pause = value;
            }
        }
        private bool pause;
        public Tile PacTarget;
        public NavTile[,] MovementGrid = new NavTile[19, 25];

        public Vector3 NextTile(PacDirection direction, Tile currentTile, out Tile nextTile)
        {
            NavTile returnTile = new NavTile(0, 0);
            nextTile = new Tile(0, 0);

            switch (direction)
            {
                case PacDirection.Up:
                    if (currentTile.Y == 0)
                    {
                        nextTile = currentTile;
                    }
                    else
                    {
                        nextTile = currentTile.Add(new Tile(0, -1));
                    }

                    returnTile = MovementGrid[nextTile.X, nextTile.Y];
                    break;
                case PacDirection.Down:
                    if (currentTile.Y + 1 == MovementGrid.GetLength(1))
                    {
                        nextTile = currentTile;
                    }
                    else
                    {
                        nextTile = currentTile.Add(new Tile(0, 1));
                    }

                    returnTile = MovementGrid[nextTile.X, nextTile.Y];
                    break;
                case PacDirection.Left:
                    if (currentTile.X == 0)
                    {
                        nextTile = currentTile;
                    }
                    else
                    {
                        nextTile = currentTile.Add(new Tile(-1, 0));
                    }

                    returnTile = MovementGrid[nextTile.X, nextTile.Y];
                    break;
                case PacDirection.Right:
                    if (currentTile.X + 1 == MovementGrid.GetLength(0))
                    {
                        nextTile = currentTile;
                    }
                    else
                    {
                        nextTile = currentTile.Add(new Tile(1, 0));
                    }

                    returnTile = MovementGrid[nextTile.X, nextTile.Y];
                    break;
            }

            if (returnTile.IsZero || !returnTile.IsNav)
            {
                returnTile = MovementGrid[currentTile.X, currentTile.Y];
                nextTile = currentTile;
            }

            PacTarget = nextTile;
            return returnTile.toVector3();
        }

        public Transform GetSpawn()
        {
            var transform = (from a in SpawnPoints
                             join b in Ghosts on a.position equals b.transform.position into br
                             from brr in br.DefaultIfEmpty()
                             where brr == null
                             select a).FirstOrDefault();

            if (transform == null)
            {
                var ghost = Ghosts.FirstOrDefault(x => x.transform.position == SpawnPoints[0].position);
                ghost.IsDead = false;
                return SpawnPoints[0];
            }

            return transform;
        }

        IEnumerator SpawnGhosts()
        {
            CoroutineManager.Instance.StartCoroutine(Ghosts[ghostsSpawned].BeginRespawn());

            yield return new WaitForSeconds(3);
            ghostsSpawned++;
            if (ghostsSpawned < 4)
            {
                CoroutineManager.Instance.StartCoroutine(SpawnGhosts());
            }
        }

        public IEnumerator GhostsVulnerable()
        {
            foreach (var a in ghosts.Where(x => !x.IsDead))
            {
                a.IsVulnerable = true;
            }

            yield return new WaitForSeconds(2);

            foreach (var a in ghosts.Where(x => !x.IsDead))
            {
                a.IsVulnerable = false;
            }
        }

        public Vector3 GetPathToRandomSquare()
        {
            return MovementGrid[UnityEngine.Random.Range(0, 19), UnityEngine.Random.Range(0, 27)].toVector3();
        }

        #region navPoints
        static int[,] navPointsMap = new int[,] {
            {1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1},
{1,0,0,0,1,0,0,0,1,0,1,0,0,0,1,0,0,0,1},
{1,0,0,0,1,0,0,0,1,0,1,0,0,0,1,0,0,0,1},
{1,0,0,0,1,0,0,0,1,0,1,0,0,0,1,0,0,0,1},
{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
{1,0,0,0,1,0,1,0,0,0,0,0,1,0,1,0,0,0,1},
{1,0,0,0,1,0,1,0,0,0,0,0,1,0,1,0,0,0,1},
{1,1,1,1,1,0,1,1,1,0,1,1,1,0,1,1,1,1,1},
{0,0,0,0,1,0,0,0,1,0,1,0,0,0,1,0,0,0,0},
{1,1,1,0,1,0,1,1,1,1,1,1,1,0,1,0,1,1,1},
{1,1,1,0,1,0,1,0,0,1,0,0,1,0,1,0,1,1,1},
{0,0,0,0,1,0,1,0,1,1,1,0,1,0,1,0,0,0,0},
{1,1,1,1,1,1,1,0,1,1,1,0,1,1,1,1,1,1,1},
{0,0,0,0,1,0,1,0,0,0,0,0,1,0,1,0,0,0,0},
{1,1,1,0,1,0,1,1,1,1,1,1,1,0,1,0,1,1,1},
{1,1,1,0,1,0,1,0,0,0,0,0,1,0,1,0,1,1,1},
{0,0,0,0,1,0,1,0,0,0,0,0,1,0,1,0,0,0,0},
{1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1},
{1,0,0,0,1,0,0,0,1,0,1,0,0,0,1,0,0,0,1},
{1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1},
{0,0,1,0,1,0,1,0,0,0,0,0,1,0,1,0,1,0,0},
{0,0,1,0,1,0,1,0,0,0,0,0,1,0,1,0,1,0,0},
{1,1,1,1,1,0,1,1,1,0,1,1,1,0,1,1,1,1,1},
{1,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,1},
{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
        };
        #endregion
        public void Awake()
        {
            for (int i = 0; i < 19; i++)
            {
                for (int j = 0; j < 25; j++)
                {
                    try
                    {
                        float x = -0.72f + (0.08f * i);
                        float y = 0.96f + (-0.08f * j);
                        MovementGrid[i, j] = new NavTile(x, y, navPointsMap[j, i] == 1);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }

            CoroutineManager.Instance.StartCoroutine(SpawnGhosts());
        }

        public void Update()
        {
        }

        public void GameOver()
        {
            Pause = true;
            pausePanel.ShowGameOverPanel();
        }

        private void ResetObjects(bool ResetCollectibles)
        {
            CoroutineManager.Instance.SetActive(false);
            Destroy(PacMan.gameObject);

            for (int i = 0; i < Ghosts.Count; i++)
            {
                Destroy(Ghosts[i].gameObject);
            }

            ghosts = new List<GhostAi>();
            Instantiate(Resources.Load("Pac-Man"));
            Ghosts.Add(((GameObject)Instantiate(Resources.Load("Blinky"))).GetComponent<GhostAi>());
            Ghosts.Add(((GameObject)Instantiate(Resources.Load("Pinky"))).GetComponent<GhostAi>());
            Ghosts.Add(((GameObject)Instantiate(Resources.Load("Inky"))).GetComponent<GhostAi>());
            Ghosts.Add(((GameObject)Instantiate(Resources.Load("Clyde"))).GetComponent<GhostAi>());

            if (ResetCollectibles)
            {
                Destroy(CollectibleContainer);
                CollectibleContainer = ((GameObject)Instantiate(Resources.Load("CollectibleContainer")));
            }

            ghostsSpawned = 1;
            CoroutineManager.Instance.SetActive(true);
            CoroutineManager.Instance.StartCoroutine(SpawnGhosts());
        }

        public void NextLevel()
        {
            Pause = true;
            pausePanel.ShowNextLevelPanel();
            ResetObjects(true);
            Pause = false;
        }

        public void RestartLevel()
        {
            Pause = true;
            ResetObjects(false);
            Pause = false;
        }

        void OnDisable()
        {
            this.enabled = true;
        }

        public void PauseGame(bool ShowPausePanel = false)
        {
            Pause = !Pause;

            if (ShowPausePanel)
            {
                pausePanel.ShowPausePanel();
            }
        }

        public void UnPauseGame(bool ShowPausePanel = false)
        {
            Pause = false;

            if (ShowPausePanel)
            {
                pausePanel.ShowPausePanel();
            }
        }
    }
}
