﻿using UnityEngine;
using System.Collections;
using System;


namespace Assets.Scripts.Pac_Man
{
    public class GhostAi : PacUnit
    {
        private bool isVulnerable;
        public bool IsVulnerable
        {
            set
            {
                animator.SetBool("Vulnerable", value);
                isVulnerable = value;
            }
            get { return isVulnerable; }
        }
        public int Score;
        public bool isDead;
        public bool IsDead
        {
            set
            {
                if (value)
                {
                    AiLerp.target = gameController.GetSpawn();
                    animator.SetBool("IsDead", true);
                }
                else
                {
                    AiLerp.target = PacMan.transform;
                    Pause(false);
                }

                isDead = value;
            }
            get { return isDead; }
        }
        public Ghost ghost;
        private AILerp aiLerp;
        private AILerp AiLerp
        {
            get
            {
                if (aiLerp == null && this != null)
                {
                    aiLerp = GetComponent<AILerp>();
                }

                return aiLerp;
            }
        }
        private PacManPlayerController pacMan;
        private PacManPlayerController PacMan
        {
            get
            {
                if (pacMan == null)
                {
                    pacMan = FindObjectOfType<PacManPlayerController>();
                }

                return pacMan;
            }
        }

        public override void StartFunctions()
        {
        }

        void LateUpdate()
        {
            if (!IsDead)
            {
                switch (ghost)
                {
                    case Ghost.Blinky:
                        if (AiLerp.target == default(Transform) && PacMan != null)
                        {
                            AiLerp.target = PacMan.transform;
                        }
                        break;
                    case Ghost.Clyde:
                        if (AiLerp.target == default(Transform) && PacMan != null)
                        {
                            AiLerp.target = PacMan.transform;
                        }
                        break;
                    case Ghost.Inky:
                        if (AiLerp.target == default(Transform) && PacMan != null)
                        {
                            AiLerp.target = PacMan.transform;
                        }
                        break;
                    case Ghost.Pinky:
                        if (AiLerp.target == default(Transform) && PacMan != null)
                        {
                            AiLerp.target = PacMan.transform;
                        }
                        break;
                }
            }
            else if (AiLerp.target != null && (AiLerp.target.position == transform.position))
            {
                Pause(true);
                CoroutineManager.Instance.StartCoroutine(BeginRespawn());
            }

            if (gameController.Pause && AiLerp.canMove)
            {
                Pause(true);
            }
            else if (!gameController.Pause && !AiLerp.canMove)
            {
                Pause(false);
            }
        }

        public void Pause(bool pauseState)
        {
            AiLerp.canMove = !pauseState;
            AiLerp.canSearch = !pauseState;
        }

        void Update()
        {

        }

        public void ReturnToSpawn()
        {
            Pause(true);
            IsDead = true;
            animator.SetBool("Dead", true);
            Pause(false);
        }

        void OnCollisionEnter2D(Collision2D col)
        {
            if (col.gameObject.GetComponent<Boundary>() != null)
            {
                col.gameObject.GetComponent<Boundary>().SwapBoundary(this.transform, this);
            }
        }

        public IEnumerator BeginRespawn()
        {
            yield return new WaitForSeconds(3);
            Spawn();
        }

        public void Spawn()
        {
            animator.SetBool("Dead", false);
            IsDead = false;
            Pause(false);
        }
    }

    public enum Ghost
    {
        Blinky,
        Pinky,
        Inky,
        Clyde
    }
}