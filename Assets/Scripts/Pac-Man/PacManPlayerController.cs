﻿using UnityEngine;
using System.Collections;
using System;

namespace Assets.Scripts.Pac_Man
{
    public class PacManPlayerController : PacUnit
    {
        public float speed;
        public AudioClip deathSound;
        public AudioClip eatSound;
        public PacDirection direction;
        public PacDirection prevDirection;
        private bool posChangedLastMove;
        private Vector3 targetPosition;
        private Tile currentTile = new Tile(9, 19);

        public override void StartFunctions()
        {

        }

        void FixedUpdate()
        {
            if (!gameController.Pause)
            {
                UpdatePosition();
            }
        }

        public void UpdatePosition()
        {
            if (targetPosition == Vector3.zero || (targetPosition.x == transform.position.x && targetPosition.y == transform.position.y))
            {
                targetPosition = gameController.NextTile(direction, currentTile, out currentTile);

                if (targetPosition != transform.position)
                {
                    SetAnimation();
                }
                else if (posChangedLastMove)
                {
                    direction = prevDirection;
                }

                posChangedLastMove = false;
            }

            transform.position = Vector3.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);
        }

        void Update()
        {
            if (Input.GetKeyDown("escape"))//pause button
            {
                gameController.PauseGame(true);
            }

            if (Input.GetKeyDown("d") && direction != PacDirection.Right)
            {
                if (direction != PacDirection.Right)
                {
                    prevDirection = direction;
                    posChangedLastMove = true;
                }

                direction = PacDirection.Right;
            }
            else if (Input.GetKeyDown("a") && direction != PacDirection.Left)
            {
                if (direction != PacDirection.Left)
                {
                    prevDirection = direction;
                    posChangedLastMove = true;
                }

                direction = PacDirection.Left;
            }
            else if (Input.GetKeyDown("w") && direction != PacDirection.Up)
            {
                if (direction != PacDirection.Up)
                {
                    prevDirection = direction;
                    posChangedLastMove = true;
                }

                direction = PacDirection.Up;
            }
            else if (Input.GetKeyDown("s") && direction != PacDirection.Down)
            {
                if (direction != PacDirection.Down)
                {
                    prevDirection = direction;
                    posChangedLastMove = true;
                }

                direction = PacDirection.Down;
            }
        }

        void SetAnimation()
        {
            switch (direction)
            {
                case PacDirection.Right:
                    animator.SetBool("Left", false);
                    animator.SetBool("Up", false);
                    animator.SetBool("Down", false);
                    break;
                case PacDirection.Left:
                    animator.SetBool("Left", true);
                    animator.SetBool("Up", false);
                    animator.SetBool("Down", false);
                    break;
                case PacDirection.Up:
                    animator.SetBool("Left", false);
                    animator.SetBool("Up", true);
                    animator.SetBool("Down", false);
                    break;
                case PacDirection.Down:
                    animator.SetBool("Left", false);
                    animator.SetBool("Up", false);
                    animator.SetBool("Down", true);
                    break;
            }
        }

        void OnCollisionEnter2D(Collision2D col)
        {
            if (col.gameObject.GetComponent<PacCollectible>() != null)//increment score by amount the collectible is worth
            {
                gameController.ScorePanel.ChangeScore(col.gameObject.GetComponent<PacCollectible>().Score);
                if (col.gameObject.CompareTag("PowerUp"))
                {
                   CoroutineManager.Instance.StartCoroutine(gameController.GhostsVulnerable());
                }

                Destroy(col.gameObject);
            }
            else if (col.gameObject.GetComponent<Boundary>() != null)//reached a boundary object
            {
                col.gameObject.GetComponent<Boundary>().SwapBoundary(this.transform, direction, out currentTile, out targetPosition);
            }
            else if (col.gameObject.GetComponent<GhostAi>() != null)
            {
                var ghost = col.gameObject.GetComponent<GhostAi>();
                if (!ghost.IsVulnerable)
                {
                    rigidBody2d.constraints = RigidbodyConstraints2D.FreezePosition;
                    animator.SetBool("Death", true);
                    audioSource.clip = deathSound;
                    audioSource.Play();
                    CoroutineManager.Instance.StartCoroutine(WaitForAudio(new Action<int>(gameController.ScorePanel.ChangeLives), -1));
                }
                else
                {
                    gameController.ScorePanel.ChangeScore(ghost.Score);
                    ghost.ReturnToSpawn();
                }
            }
            else
            {
                direction = prevDirection;
                targetPosition = gameController.NextTile(direction, currentTile, out currentTile);
            }
        }
    }

    public enum PacDirection
    {
        Right,
        Left,
        Up,
        Down
    }
}