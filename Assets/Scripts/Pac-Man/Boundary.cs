﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Pac_Man
{
    public class Boundary : MonoBehaviour
    {
        public Boundary targetBoundary;
        public Vector3 SpawnLocation;
        public PacGameController gameController;
        public void SwapBoundary(Transform targetTransform, PacDirection direction, out Tile nextTile, out Vector3 targetPosition)
        {
            if (direction == PacDirection.Left)
            {
                targetPosition = gameController.NextTile(direction, new Tile(18, 12), out nextTile);
            }
            else
            {
                targetPosition = gameController.NextTile(direction, new Tile(0, 12), out nextTile);
            }

            targetTransform.position = targetBoundary.SpawnLocation;
        }

        public void SwapBoundary(Transform targetTransform, GhostAi ghost)
        {
            ghost.Pause(true);
            targetTransform.position = targetBoundary.SpawnLocation;
            ghost.Pause(false);
        }
    }
}
