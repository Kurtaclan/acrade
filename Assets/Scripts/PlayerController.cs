﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class PlayerController : MonoBehaviour
{
    public float speed;
    private bool canDie = true;
    public GameObject bullet;
    public Sprite explosion;
    public Sprite baseSprite;
    bool canFire = true;
    public EnemyController controller;
    public PausePanel pause;
    public AudioClip deathSound;

    void LateUpdate()
    {
        if (Input.GetButtonDown("Fire1") && canFire)
        {
            StartCoroutine(ShotFired());
        }
    }

    IEnumerator ShotFired()
    {
        canFire = false;
        var center = this.GetComponent<SpriteRenderer>().bounds.center;
        Instantiate(bullet, center + new Vector3(0, 0.3f, 0), Quaternion.Euler(0, 0, 0));

        yield return new WaitForSeconds(0.9f);
        canFire = true;
    }


    void FixedUpdate()
    {
        float movex = Input.GetAxis("Horizontal") * speed;
        movex *= Time.deltaTime;
        transform.Translate(movex, 0, 0);

        if (movex != 0)
        {
            float widthRel = (this.GetComponent<BoxCollider2D>().size.x / (Camera.main.pixelWidth)) * 400;
            Vector3 viewPos = Camera.main.WorldToViewportPoint(this.transform.position);
            viewPos.x = Mathf.Clamp(viewPos.x, widthRel, 1 - widthRel);
            this.transform.position = Camera.main.ViewportToWorldPoint(viewPos);
        }
    }

    void Update()
    {
        if (Input.GetKeyDown("escape"))
        {
            Time.timeScale = (Time.timeScale == 1 ? 0 : 1);
            pause.ShowPausePanel();
        }
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.name.Contains("EnemyShot"))
        {
            Destroy(col.gameObject);
        }

        if (canDie)
        {
            PlayerDeath();
        }
    }

    public void PlayerDeath()
    {
        controller.Pause = true;
        this.GetComponent<SpriteRenderer>().sprite = explosion;
        this.GetComponent<AudioSource>().Play();
        var livesText = GameObject.Find("LivesCounter").GetComponent<Text>();
        var lives = Int32.Parse(livesText.text.Substring(1, livesText.text.Length - 1));
        if (lives == 0)
        {
            pause.ShowGameOverPanel();
        }
        else
        {
            livesText.text = "x " + (lives - 1).ToString();
            StartCoroutine(RestartRound());
        }
    }

    IEnumerator RestartRound()
    {
        yield return new WaitForSeconds(1f);
        this.GetComponent<SpriteRenderer>().sprite = baseSprite;        
        Time.timeScale = 0;
        controller.ResetPosition();
        Time.timeScale = 1;
        canDie = true;
    }
}
