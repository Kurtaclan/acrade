﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class PausePanel : MonoBehaviour
{
    public Text GameOver;
    public Text Pause;
    public Text NextLevel;

    void Start()
    {
        this.gameObject.SetActive(false);
    }

    public void ShowPausePanel()
    {
        GameOver.enabled = false;
        Pause.enabled = true;
        if (NextLevel != null)
            NextLevel.enabled = false;
        this.gameObject.SetActive(!this.gameObject.activeSelf);
    }

    public void ShowGameOverPanel()
    {
        Pause.enabled = false;
        GameOver.enabled = true;
        if (NextLevel != null)
            NextLevel.enabled = false;
        this.gameObject.SetActive(true);
    }

    public void ShowNextLevelPanel()
    {
        Pause.enabled = false;
        GameOver.enabled = false;
        if (NextLevel != null)
            NextLevel.enabled = true;
        this.gameObject.SetActive(true);
    }
}
