﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class ScoreDisplay : MonoBehaviour
{   
    public void DisplayScore(int scoreToAdd)
    {
        this.GetComponent<AudioSource>().Play();
        this.GetComponent<Text>().text = scoreToAdd.ToString();
        var scoreText = GameObject.Find("CurrentScore").GetComponent<Text>();
        var score = Int32.Parse(scoreText.text);
        score += 200;
        scoreText.text = score.ToString("D16");
        Destroy(this.gameObject, 0.3f);
    }
}
