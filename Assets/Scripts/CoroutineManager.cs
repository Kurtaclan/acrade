﻿using UnityEngine;
using System.Collections;
using System;

public class CoroutineManager : MonoBehaviour
{
    private static CoroutineManager instance = null;
    public static CoroutineManager Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType<CoroutineManager>();

            return instance;
        }
    }

    internal void SetActive(bool activeSet)
    {
        gameObject.SetActive(activeSet);
    }
}
