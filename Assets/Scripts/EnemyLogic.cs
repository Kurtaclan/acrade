﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class EnemyLogic : MonoBehaviour
{
    public GameObject bullet;
    public GameObject ScoreDisplay;
    bool canFire = false;
    public int Score;

    void Awake()
    {
        StartCoroutine(EnableFire());
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.name.Contains("EnemyShot"))
        {
            Physics2D.IgnoreCollision(col.collider, GetComponent<Collider2D>());
        }
        else if (col.gameObject.name.Contains("Shot"))
        {
            col.gameObject.GetComponent<ShotLogic>().Explode();
            Destroy(col.gameObject, 0.03f);

            var display = (GameObject)Instantiate(ScoreDisplay, GameObject.Find("Canvas").transform);
            display.transform.position = Camera.main.WorldToScreenPoint(transform.position);
            display.SendMessage("DisplayScore", Score);

            Destroy(this.gameObject);
        }
        else if (col.gameObject.name.Contains("Barrier"))
        {
            Destroy(col.gameObject);
            Destroy(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    void LateUpdate()
    {
        if ((Camera.main.WorldToViewportPoint(transform.position).x > 0.9f && transform.parent.GetComponent<EnemyController>().speed > 0)
            || (Camera.main.WorldToViewportPoint(transform.position).x < 0.1f && transform.parent.GetComponent<EnemyController>().speed < 0))
        {
            transform.parent.GetComponent<EnemyController>().ReverseDirection();
        }

        if (Camera.main.WorldToViewportPoint(transform.position).y < 0)
        {
            GameObject.Find("Player").GetComponent<PlayerController>().PlayerDeath();
        }

        if (canFire)
        {
            StartCoroutine(ShotTest());
        }
    }

    IEnumerator EnableFire()
    {
        yield return new WaitForSeconds(2);
        canFire = true;
    }

    IEnumerator ShotTest()
    {
        canFire = false;
        if (UnityEngine.Random.Range(1, 100) > 93)
        {
            var center = this.GetComponent<SpriteRenderer>().bounds.center;
            Instantiate(bullet, center - new Vector3(0, 0.3f, 0), Quaternion.Euler(0, 0, 0));
        }

        yield return new WaitForSeconds(4);
        canFire = true;
    }
}
