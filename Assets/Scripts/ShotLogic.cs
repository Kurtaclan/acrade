﻿using UnityEngine;
using System.Collections;

public class ShotLogic : MonoBehaviour
{
    public float speed;
    public Sprite explosion;
    void FixedUpdate()
    {
        float movey = speed;
        movey *= Time.deltaTime;
        transform.Translate(0, movey, 0);

        Vector3 viewPos = Camera.main.WorldToViewportPoint(this.transform.position);
        if (viewPos.y > 1)
        {
            Destroy(this.gameObject);
        }
    }

    public void Explode()
    {
        this.GetComponent<SpriteRenderer>().sprite = explosion;
    }
}
