﻿using System;
using UnityEngine;

public class QBertCube : MonoBehaviour
{
    public Color TopColor { get { return TopColorRenderer.color; } set { TopColorRenderer.color = value; } }
    public Color BottomColor { get { return BottomColorRenderer.color; } set { BottomColorRenderer.color = value; } }
    private SpriteRenderer TopColorRenderer;
    private SpriteRenderer BottomColorRenderer;
    public bool Flipped = false;

    private void Start()
    {
        TopColorRenderer = transform.GetChild(0).GetComponent<SpriteRenderer>();
        BottomColorRenderer = transform.GetChild(1).GetComponent<SpriteRenderer>();
    }

    public void SteppedOn(bool loopColors) {        
        if (loopColors)
        {
            Flipped = !Flipped;
            TopColor = new Color(1.0f - TopColor.r, 1.0f - TopColor.g, 1.0f - TopColor.b);
        }
        else if(!Flipped)
        {
            Flipped = true;
            TopColor = new Color(1.0f - TopColor.r, 1.0f - TopColor.g, 1.0f - TopColor.b);
        }
    }
}
