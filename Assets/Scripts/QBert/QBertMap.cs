﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class QBertMap : MonoBehaviour
{
    public QBertRow[] Rows;
    public QBertCube[][] Cubes;
    public Transform SpawnPoint;
    public Transform QBert;
    public Animator QBertAnimator;
    public GameObject NextLevelCanvas;
    public GameObject GameOverCanvas;
    public Vector2 CurrentPosition = new Vector2(1, 1);
    internal Color topColor;
    internal Color bottomColor;
    private bool NextLevelCheck = false;
    public Text Level;
    private int LevelNumber = 0;
    public Text Lives;
    private int LiveCount = 3;

    public Color TopColor
    {
        get { return topColor; }
        set
        {
            topColor = value;
            foreach (var row in Cubes)
            {
                foreach (var cube in row)
                {
                    cube.TopColor = topColor;
                    cube.Flipped = false;
                }
            }
        }
    }
    public Color BottomColor
    {
        get { return bottomColor; }
        set
        {
            bottomColor = value;
            foreach (var row in Cubes)
            {
                foreach (var cube in row)
                {
                    cube.BottomColor = bottomColor;
                }
            }
        }
    }

    public bool LoopColors = false;
    void Start()
    {
        Rows = GetComponentsInChildren<QBertRow>();

        foreach (var row in Rows)
        {
            row.SetupRow();
        }

        Cubes = Rows.Select(x => x.Cubes).ToArray();
        StartCoroutine(NextLevel());
    }

    private void Update()
    {
        if (Cubes.Count(x => x.Count(y => y.Flipped) == x.Length) == Cubes.Length && !NextLevelCheck)
        {
            StartCoroutine(NextLevel());
        }
    }

    private IEnumerator NextLevel()
    {
        NextLevelCheck = true;
        NextLevelCanvas.SetActive(true);
        GameOverCanvas.SetActive(false);
        LevelNumber++;
        Level.text = LevelNumber.ToString();
        LoopColors = UnityEngine.Random.value > 0.5f;
        QBertAnimator.SetBool("Front_Left", true);
        QBertAnimator.SetBool("Back_Left", false);
        QBertAnimator.SetBool("Front_Right", false);
        QBertAnimator.SetBool("Back_Right", false);

        yield return new WaitForSeconds(0.3f);
        TopColor = UnityEngine.Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
        BottomColor = UnityEngine.Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
        QBert.position = SpawnPoint.position;
        NextLevelCanvas.SetActive(false);
        NextLevelCheck = false;
    }

    #region Buttons
    public void TestUpLeft()
    {
        QBertAnimator.SetBool("Front_Left", false);
        QBertAnimator.SetBool("Back_Left", true);
        QBertAnimator.SetBool("Front_Right", false);
        QBertAnimator.SetBool("Back_Right", false);
        NavigateMap(MapDirection.UP_LEFT);
    }

    public void TestUpRight()
    {
        QBertAnimator.SetBool("Front_Left", false);
        QBertAnimator.SetBool("Back_Left", false);
        QBertAnimator.SetBool("Front_Right", false);
        QBertAnimator.SetBool("Back_Right", true);
        NavigateMap(MapDirection.UP_RIGHT);
    }

    public void TestDownLeft()
    {
        QBertAnimator.SetBool("Front_Left", true);
        QBertAnimator.SetBool("Back_Left", false);
        QBertAnimator.SetBool("Front_Right", false);
        QBertAnimator.SetBool("Back_Right", false);
        NavigateMap(MapDirection.DOWN_LEFT);
    }

    public void TestDownRight()
    {
        QBertAnimator.SetBool("Front_Left", false);
        QBertAnimator.SetBool("Back_Left", false);
        QBertAnimator.SetBool("Front_Right", true);
        QBertAnimator.SetBool("Back_Right", false);
        NavigateMap(MapDirection.DOWN_RIGHT);
    }
    #endregion

    public Vector3 NavigateMap(MapDirection direction)
    {
        switch (direction)
        {
            case MapDirection.UP_LEFT:
                return MoveQBert((int)CurrentPosition.x - 1, (int)CurrentPosition.y - 1);
            case MapDirection.UP_RIGHT:
                return MoveQBert((int)CurrentPosition.x, (int)CurrentPosition.y - 1);
            case MapDirection.DOWN_LEFT:
                return MoveQBert((int)CurrentPosition.x, (int)CurrentPosition.y + 1);
            case MapDirection.DOWN_RIGHT:
                return MoveQBert((int)CurrentPosition.x + 1, (int)CurrentPosition.y + 1);
            default:
                return SpawnPoint.transform.position;
        }
    }

    public Vector3 MoveQBert(int column, int row)
    {
        if (row == 0 || column == 0 || row > Cubes.Length || column > Cubes[row - 1].Length)
        {
            CurrentPosition = new Vector2(1, 1);
            LiveCount--;
            Lives.text = LiveCount.ToString() + " x";
            QBertAnimator.SetBool("Front_Left", true);
            QBertAnimator.SetBool("Back_Left", false);
            QBertAnimator.SetBool("Front_Right", false);
            QBertAnimator.SetBool("Back_Right", false);

            if(LiveCount == 0)
            {
                GameOverCanvas.SetActive(true);
            }

            return QBert.position = SpawnPoint.position;
        }
        else
        {
            CurrentPosition = new Vector2(column, row);
            Cubes[row - 1][column - 1].SteppedOn(LoopColors);
            return QBert.position = Cubes[row - 1][column - 1].transform.position + new Vector3(0, .63f, 0);
        }
    }

    public void RestartLevel()
    {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }
}

public enum MapDirection
{
    UP_LEFT,
    UP_RIGHT,
    DOWN_LEFT,
    DOWN_RIGHT
}