﻿using UnityEngine;
using System.Collections;

public class BarrierController : MonoBehaviour
{
    public int health = 3;
    public Sprite[] sprites;
    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.name.Contains("Shot"))
        {
            if (col.gameObject.name.Contains("EnemyShot"))
            {
                col.gameObject.GetComponent<EnemyShotLogic>().Explode();
            }
            else
            {
                col.gameObject.GetComponent<ShotLogic>().Explode();
            }

            Destroy(col.gameObject, 0.03f);
            health--;

            if (health == 0)
            {
                Destroy(this.gameObject);
            }
            else
            {
                this.GetComponent<SpriteRenderer>().sprite = sprites[3 - health];
            }
        }
    }
}
