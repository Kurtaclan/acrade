﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnemyController : MonoBehaviour
{
    public GameObject[] BasicEnemies;
    public GameObject MotherShip;
    public GameObject TempShip;
    public int WaveCount;
    public float speed;
    public float curSpeed;
    private bool canSpawn;
    public bool Pause;
    private Vector3 startPos;
    public Text RoundText;
    public GameObject RoundPanel;
    public int round = 1;
    private AudioSource audioS;

    void Start()
    {
        audioS = this.GetComponent<AudioSource>();
        startPos = this.transform.position + new Vector3(0, 0, 10);
        Pause = true;
        StartCoroutine(NextRound());
        StartCoroutine(EnableSpawn());
    }

    public void DestroyShots()
    {
        var gameObjects = GameObject.FindGameObjectsWithTag("Shot");

        for (var i = 0; i < gameObjects.Length; i++)
            Destroy(gameObjects[i]);
    }

    public void ResetPosition()
    {
        DestroyShots();
        this.transform.position = startPos;
        Pause = false;
    }

    IEnumerator NextRound()
    {
        DestroyShots();
        audioS.pitch = 1;
        RoundPanel.SetActive(true);
        RoundText.text = "Round " + round.ToString();

        yield return new WaitForSeconds(1);

        RoundPanel.SetActive(false);

        if (transform.childCount > 0)
        {
            foreach (Transform child in transform)
            {
                Destroy(child.gameObject);
            }
        }

        this.transform.position = Camera.main.WorldToViewportPoint(new Vector2(0, 1));

        var f = 1;
        var j = 0;
        for (int i = 1; i <= WaveCount; i++)
        {
            var enemy = Instantiate(BasicEnemies[Random.Range(0, BasicEnemies.Length - 1)], this.transform);
            if (0.08f * f >= 0.9f)
            {
                j++;
                f = 1;
            }

            var pos = Camera.main.ViewportToWorldPoint(new Vector3(0.06f * f, 0.85f - (j * .06f), 0));
            pos.z = 0;
            ((GameObject)enemy).transform.position = pos;
            f++;
        }

        Pause = false;
        StartCoroutine(SoundLoop());
    }

    IEnumerator SoundLoop()
    {
        while (true)
        {
            audioS.Play();
            yield return new WaitForSeconds(0.5f);
        }
    }

    void Update()
    {
        if (transform.childCount == 0 && !Pause)
        {
            round++;
            Pause = true;
            StartCoroutine(NextRound());
        }
    }

    void LateUpdate()
    {
        if (!Pause)
        {
            if (WaveCount > transform.childCount && transform.childCount != 0)
            {
                curSpeed = speed * Mathf.Lerp(1, 12f, 1 - (((float)transform.childCount) / ((float)WaveCount)));
                audioS.pitch = 1 + Mathf.Lerp(0, 1.5f, 1 - (((float)transform.childCount) / ((float)WaveCount)));
            }
            else
            {
                curSpeed = speed;
            }

            if (canSpawn && TempShip == null)
            {
                StartCoroutine(MotherShipTest());
            }

            int direction = 1;
            float movex = curSpeed;
            movex *= Time.deltaTime;

            transform.Translate(movex * direction, 0, 0);
        }
    }

    IEnumerator MotherShipTest()
    {
        canSpawn = false;
        if (Random.Range(0, 100) > 70)
        {
            TempShip = (GameObject)Instantiate(MotherShip, Camera.main.ViewportToWorldPoint(new Vector3(0, 0.9f, 0)) + new Vector3(0, 0, 11), Quaternion.Euler(0, 0, 0));
        }

        yield return new WaitForSeconds(15);
        canSpawn = true;
    }

    IEnumerator EnableSpawn()
    {
        yield return new WaitForSeconds(4);
        canSpawn = true;
    }

    public void ReverseDirection()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y - 1, transform.position.z);
        speed *= -1;
    }
}
