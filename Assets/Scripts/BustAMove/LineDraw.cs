﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineDraw : MonoBehaviour
{
    LineRenderer lineRenderer;
    public Vector3 currentPosition;
    public float circleSize = 0.1f;
    private static LineDraw instance = null;
    public static LineDraw Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType<LineDraw>();

            return instance;
        }
    }

    void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }

    public void Update()
    {
        if (currentPosition != transform.position)
        {
            lineRenderer.positionCount = 1;
            lineRenderer.SetPosition(0, transform.position);
            int i = 1;
            RaycastHit2D hit = Physics2D.CircleCast(transform.position, circleSize, transform.up, Mathf.Infinity);

            while (i < 8 && hit.collider != null && !hit.collider.gameObject.name.Contains("Ball"))
            {
                lineRenderer.positionCount = i + 1;
                lineRenderer.SetPosition(i, hit.point);

                Vector3 inDirection = Vector3.Reflect(transform.up, hit.normal);
                hit = Physics2D.CircleCast(hit.point + hit.normal * circleSize, circleSize, inDirection, Mathf.Infinity);
                i++;

                if (hit.collider != null && !hit.collider.gameObject.name.Contains("Ball"))
                {
                    lineRenderer.positionCount = i + 1;
                    lineRenderer.SetPosition(i, hit.point);

                    inDirection = Vector3.Reflect(inDirection, hit.normal);
                    hit = Physics2D.CircleCast(hit.point + hit.normal * circleSize, circleSize, inDirection, Mathf.Infinity);
                    i++;
                }
            }

            lineRenderer.positionCount = i + 1;
            lineRenderer.SetPosition(i, hit.point);

            currentPosition = transform.position;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawCube(transform.position, new Vector3(0.1f, 0.1f, 0));

        if (Application.isPlaying)
        {
            for (int i = 0; i < lineRenderer.positionCount; i++)
            {
                Gizmos.DrawSphere(lineRenderer.GetPosition(i), circleSize);
            }
        }
    }
}
