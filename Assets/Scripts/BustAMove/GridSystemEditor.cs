﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

using System.Collections;

[CustomEditor(typeof(GridSystem))]
public class GridSystemEditor : Editor
{
    GridSystem _target;

    void OnEnable()
    {
        _target = (GridSystem)target;
    }

    public override void OnInspectorGUI()
    {
        GUILayout.BeginVertical();
        _target.gridType = (GridType)EditorGUILayout.EnumPopup("Grid type", _target.gridType);

        GUILayout.Space(10);

        GUILayout.Label("Grid System Configuration:", EditorStyles.boldLabel);
        GUILayout.Label("For Hex Grids, enabling stagger switches to the second row/column.");
        GUILayout.BeginHorizontal();
        _target.StaggerRows = EditorGUILayout.Toggle("Stagger Row(s)?", _target.StaggerRows);
        if (GUI.changed && _target.StaggerRows)
        {
            _target.StaggerColumns = false;
        }

        _target.StaggerColumns = EditorGUILayout.Toggle("Stagger Column(s)?", _target.StaggerColumns);
        if (GUI.changed && _target.StaggerColumns)
        {
            _target.StaggerRows = false;
        }

        GUILayout.EndHorizontal();
        _target.StaggerPercent = EditorGUILayout.Slider("Stagger Percent", _target.StaggerPercent, 1, 100);
        GUILayout.BeginHorizontal();
        _target.GridRows = EditorGUILayout.IntField("Row(s) count", _target.GridRows);
        _target.GridColumns = EditorGUILayout.IntField("Column(s) count", _target.GridColumns);
        GUILayout.EndHorizontal();

        if (_target.GridColumns == 0 || _target.GridRows == 0)
        {
            EditorGUILayout.HelpBox("At least one row and column must be set", MessageType.Error);
        }

        _target.GridSize = EditorGUILayout.Vector3Field("Grid node size", _target.GridSize);
        _target.GridRotation = EditorGUILayout.Vector3Field("Grid Rotation", _target.GridRotation);
        _target.SpaceBetweenNodes = EditorGUILayout.Vector2Field("Space Between Nodes", _target.SpaceBetweenNodes);

        GUILayout.Space(15);
        _target.IsGridVisibleInEditor = EditorGUILayout.Toggle("Draw Grid?", _target.IsGridVisibleInEditor);
        _target.GridColor = EditorGUILayout.ColorField("Grid Color", _target.GridColor); // Color Field        
        _target.GridTileSprite = (Sprite)EditorGUILayout.ObjectField("Grid Sprite", _target.GridTileSprite, typeof(Sprite), allowSceneObjects: true);

        if (GUILayout.Button("Update Grid"))
        {
            _target.GenerateGrid();
        }

        GUILayout.EndVertical();

        //If we changed the GUI aply the new values to the script
        if (GUI.changed)
        {
            EditorUtility.SetDirty(_target);
        }
    }
}
#endif