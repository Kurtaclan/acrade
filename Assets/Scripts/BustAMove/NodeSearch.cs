﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Assets.Scripts.BustAMove
{
    public enum SearchDirection
    {
        Up,
        Left,
        Right,
        Bottom
    }

    public class NodeSearch
    {
        public static int GetNodeChain(GridNode start, Type type, string fieldName, object fieldValue, out object[] chain)
        {
            List<GridNode> _chain = new List<GridNode>();
            IEnumerable<GridNode> nextNodes = start.Neighbors;
            _chain.Add(start);

            foreach (var node in nextNodes)
            {
                // Ignore non-occupied nodes
                if (!node.IsOccupied)
                    continue;

                // Ignore nodes with incorrect Occupier
                if (node.Occupier.GetType() != type)
                    continue;

                // Check if node has correct value
                if (type.GetField(fieldName).GetValue(node.Occupier).Equals(fieldValue))
                {
                    _chain.Add(node);
                    GetNodeChainInternal(node, type, fieldName, fieldValue, _chain);
                }
            }

            chain = _chain.Select(x => x.Occupier).ToArray();

            return _chain.Count;
        }

        public static int GetOccupiedNodes(SearchDirection direction, GridNode start)
        {
            switch (direction)
            {
                case SearchDirection.Bottom:
                    return start.Neighbors.Where(x => x.IsOccupied && x.GridRow > start.GridRow).Distinct().Count();
                case SearchDirection.Left:
                    return start.Neighbors.Where(x => x.IsOccupied && x.GridColumn < start.GridColumn).Distinct().Count();
                case SearchDirection.Right:
                    return start.Neighbors.Where(x => x.IsOccupied && x.GridColumn > start.GridColumn).Distinct().Count();
                case SearchDirection.Up:
                    return start.Neighbors.Where(x => x.IsOccupied && x.GridRow < start.GridRow).Distinct().Count();
                default:
                    return 0;
            }
        }

        internal static bool GetOccupiedNodesChain(SearchDirection direction, GridNode start)
        {
            if (GetOccupiedNodes(direction, start) != 0)
            {
                return true;
            }

            List<GridNode> _chain = new List<GridNode>();
            IEnumerable<GridNode> nextNodes = start.Neighbors;
            _chain.Add(start);

            foreach (var node in nextNodes)
            {
                // Ignore non-occupied nodes
                if (!node.IsOccupied)
                    continue;

                if (GetOccupiedNodes(direction, node, _chain) > 0)
                {
                    return true;
                }

                _chain.Add(node);
                bool foundDirection = false;
                GetOccupiedNodesChainInternal(direction, node, _chain, out foundDirection);

                if (foundDirection)
                {
                    return true;
                }
            }

            return false;
        }

        private static int GetOccupiedNodes(SearchDirection direction, GridNode start, List<GridNode> _chain)
        {
            switch (direction)
            {
                case SearchDirection.Bottom:
                    return start.Neighbors.Where(x => x.IsOccupied && x.GridRow > start.GridRow && !_chain.Contains(x)).Distinct().Count();
                case SearchDirection.Left:
                    return start.Neighbors.Where(x => x.IsOccupied && x.GridColumn < start.GridColumn && !_chain.Contains(x)).Distinct().Count();
                case SearchDirection.Right:
                    return start.Neighbors.Where(x => x.IsOccupied && x.GridColumn > start.GridColumn && !_chain.Contains(x)).Distinct().Count();
                case SearchDirection.Up:
                    return start.Neighbors.Where(x => x.IsOccupied && x.GridRow < start.GridRow && !_chain.Contains(x)).Distinct().Count();
                default:
                    return 0;
            }
        }

        private static void GetOccupiedNodesChainInternal(SearchDirection direction, GridNode start, List<GridNode> _chain, out bool foundDirection)
        {
            IEnumerable<GridNode> nextNodes = start.Neighbors.Where(x => !_chain.Contains(x));

            foreach (var node in nextNodes)
            {
                // Ignore non-occupied nodes
                if (!node.IsOccupied)
                    continue;

                if (GetOccupiedNodes(direction, node, _chain) > 0)
                {
                    foundDirection = true;
                    return;
                }

                _chain.Add(node);
                GetOccupiedNodesChainInternal(direction, node, _chain, out foundDirection);
            }

            foundDirection = false;
        }

        private static void GetNodeChainInternal(GridNode start, Type type, string fieldName, object fieldValue, List<GridNode> _chain)
        {
            IEnumerable<GridNode> nextNodes = start.Neighbors.Where(x => !_chain.Contains(x));

            foreach (var node in nextNodes)
            {
                // Ignore non-occupied nodes
                if (!node.IsOccupied)
                    continue;

                // Ignore nodes with incorrect Occupier
                if (node.Occupier.GetType() != type)
                    continue;

                // Check if node has correct value
                if (type.GetField(fieldName).GetValue(node.Occupier).Equals(fieldValue))
                {
                    _chain.Add(node);
                    GetNodeChainInternal(node, type, fieldName, fieldValue, _chain);
                }
            }
        }
    }
}
