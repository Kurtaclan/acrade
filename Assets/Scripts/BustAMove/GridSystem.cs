﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;

public enum GridType
{
    Square,
    Diamond,
    Hex
}

public class GridSystem : MonoBehaviour
{

    public bool StaggerRows;
    public bool StaggerColumns;
    public int GridRows;
    public int GridColumns = 3;
    public int GridSpacing = 3;
    public float StaggerPercent = 100f;
    public Vector3 GridSize = new Vector3(1, 1, 1);
    public Vector3 GridRotation = new Vector3(0, 0, 0);
    public Vector3 Center;
    public GridType gridType;
    public Color GridColor = Color.magenta;
    public bool IsGridVisibleInEditor = true;
    public Sprite GridTileSprite;
    public GridNode[] Nodes;
    public Vector2 SpaceBetweenNodes = new Vector2(0, 0);

    private static GridSystem instance = null;
    public static GridSystem Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType<GridSystem>();

            return instance;
        }
    }

    void Awake()
    {
        Nodes = GetComponentsInChildren<GridNode>();
    }

    private void Start()
    {
    }

    public Vector3 GetNearestNodePosition(Vector3 position)
    {
        if (Nodes != null)
        {
            return Nodes.OrderBy(p => Vector3.Distance(p.transform.position, position)).FirstOrDefault(x => !x.IsOccupied).transform.position;
        }
        else
        {
            return new Vector3(0, 0, 0);
        }
    }

    public GridNode GetNearestNode(Vector3 position)
    {
        if (Nodes != null)
        {
            return Nodes.OrderBy(p => Vector3.Distance(p.transform.position, position)).FirstOrDefault(x => !x.IsOccupied);
        }
        else
        {
            return null;
        }
    }

    public void GenerateGrid()
    {
        var list = GetComponentsInChildren<GridNode>();
        foreach (var a in list)
        {
            DestroyImmediate(a.gameObject);
        }

        var g = new GameObject();
        g.name = "GridNode";
        g.transform.localScale = GridSize;
        g.AddComponent<BoxCollider>();
        var grid = g.AddComponent<GridNode>();
        grid.Size = GridSize;
        grid.Sprite = GridTileSprite;
        grid.NodeColor = GridColor;

        if(GridTileSprite != null)
        {
            grid.s_Renderer = grid.gameObject.AddComponent<SpriteRenderer>();
            grid.s_Renderer.sprite = GridTileSprite;
        }

        var startX = -(((grid.Size.x + SpaceBetweenNodes.x) * (GridColumns / 2))) + (GridColumns % 2 == 0 ? grid.Size.x / 2 : 0);
        var startY = -(((grid.Size.y + SpaceBetweenNodes.y) * (GridRows / 2))) + (GridRows % 2 == 0 ? grid.Size.y / 2 : 0);

        for (int i = 0; i < GridRows; i++)
        {
            for (int j = 0; j < GridColumns; j++)
            {
                var staggerRow = (StaggerRows && i % 2 != 0) ? ((grid.Size.x + SpaceBetweenNodes.x) * StaggerPercent / 100) : 0;
                var staggerColumn = (StaggerColumns && j % 2 == 0) ? ((-grid.Size.y + SpaceBetweenNodes.y) * StaggerPercent / 100) : 0;
                grid.GridColumn = j + 1;
                grid.GridRow = i + 1;
                Instantiate(g,
                      new Vector3(
                          transform.position.x + staggerRow + startX + (StaggerRows ? (-(grid.Size.x / 2) * StaggerPercent / 100) : 0) + ((grid.Size.x + SpaceBetweenNodes.x) * j),
                          transform.position.y - (startY + staggerColumn + (StaggerColumns ? ((grid.Size.x / 2) * StaggerPercent / 100) : 0) + ((grid.Size.y + SpaceBetweenNodes.y) * i)),
                          transform.position.z),
                      Quaternion.Euler(GridRotation + (gridType == GridType.Diamond ? new Vector3(0, 0, 45) : Vector3.zero)),
                      transform);
            }
        }

        DestroyImmediate(g);
    }

    private void OnDrawGizmos()
    {
        if (IsGridVisibleInEditor)
        {
            Gizmos.DrawWireCube(transform.position, new Vector3(GridSize.x * GridColumns, GridSize.y * GridRows, GridSize.z));
        }
    }
}
