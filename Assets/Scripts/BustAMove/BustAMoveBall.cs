﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;


namespace Assets.Scripts.BustAMove
{
    public class BustAMoveBall : MonoBehaviour
    {
        public BallType _BallType;
        private GridNode Node;
        private object[] Chain;
        public bool gameStart;
        void Update()
        {
            if (Node != null && CheckFalling())
            {
                DropBall();
                Node.Occupier = null;
                Node = null;
            }
        }

        private void DropBall()
        {
            var rigid = gameObject.AddComponent<Rigidbody2D>();
            rigid.mass = 1;
            var collider = GetComponent<Collider2D>();
            collider.enabled = false;
        }

        IEnumerator CheckGameStart()
        {
            yield return new WaitForSeconds(.5f);
            gameStart = true;
        }

        private void OnDestroy()
        {
            if (Node != null)
            {
                Node.Occupier = null;
                Node = null;
            }
        }

        public void SetNode(GridNode node)
        {
            if (node.GridRow == GridSystem.Instance.GridRows)
            {
                GameController.Instance.GameOver();
            }

            Destroy(GetComponent<Rigidbody2D>());
            Node = node;
            Node.Occupier = this;
        }

        private bool CheckChain()
        {
            return NodeSearch.GetNodeChain(Node, GetType(), "_BallType", _BallType, out Chain) >= 3;
        }

        //True if falling;
        private bool CheckFalling()
        {
            if (Node.GridRow == 1)
            {
                return false;
            }

            return !NodeSearch.GetOccupiedNodesChain(SearchDirection.Up, Node);
        }

        private void OnBecameInvisible()
        {
            if (Application.isPlaying)
            {
                Destroy(this.gameObject);
            }
        }

        void OnCollisionEnter2D(Collision2D collision)
        {
            var gameObject = collision.gameObject;
            var ballClass = gameObject.GetComponent<BustAMoveBall>();
            if (ballClass != null)
            {
                if (gameObject.transform.parent != null)
                {
                    gameObject.transform.parent.DetachChildren();
                }

                if (Node == null)
                {
                    SetNode(GridSystem.Instance.GetNearestNode(transform.position));
                    transform.position = Node.transform.position;

                    if (CheckChain())
                    {
                        foreach (var a in Chain)
                        {
                            Destroy(((BustAMoveBall)a).gameObject);
                        }
                    }

                    LineDraw.Instance.currentPosition = Vector3.zero;
                }
            }
        }
    }
}
public enum BallType
{
    Black, Blue, Green, Light_Blue, Orange, Purple, Red, Yellow
}