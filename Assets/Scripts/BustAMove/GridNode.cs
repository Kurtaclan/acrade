﻿using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;
using System.Linq;

public class GridNode : MonoBehaviour
{
    public Sprite Sprite;
    private SpriteRenderer s_Renderer;
    public Vector3 Size;
    public bool IsOccupied { get { return Occupier != null; } }
    public Color NodeColor;
    public object Occupier;
    public int GridRow;
    public int GridColumn;
    public GridNode[] Neighbors { get; internal set; }

    // Use this for initialization
    void Start()
    {
        if (Sprite != null)
        {
            s_Renderer = gameObject.AddComponent<SpriteRenderer>();
            s_Renderer.sprite = Sprite;
        }

        Collider[] hitColliders = Physics.OverlapSphere(transform.position, Size.x * 1.1f);
        Neighbors = hitColliders.Select(x => x.gameObject).Where(x => x.GetComponent<GridNode>() != null && x != gameObject).Select(x => x.GetComponent<GridNode>()).ToArray();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnDrawGizmos()
    {
        if (transform.parent.GetComponent<GridSystem>().IsGridVisibleInEditor)
        {
            Gizmos.color = NodeColor;
            //Matrix4x4 rotationMatrix = Matrix4x4.TRS(Vector3.zero, transform.rotation, new Vector3(1,1,1));
            //Gizmos.matrix = rotationMatrix;
            Gizmos.DrawCube(transform.position, Size);
            Gizmos.DrawWireCube(transform.position, Size);
        }
    }
}
