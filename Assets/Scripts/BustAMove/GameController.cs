﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.BustAMove
{

    public class GameController : MonoBehaviour
    {
        public Transform LargeSprocket;
        public Transform MediumSprocket;
        public Transform SmallSprocket;
        public Transform PlayerPointer;
        public Transform Lever;
        public Transform BallSpawnPoint;
        public Transform NextBallSpawnPoint;
        public int BallSpeed;
        public GameObject[] BallPrefabs;
        public bool CanFire = true;

        private static GameController instance = null;
        public static GameController Instance
        {
            get
            {
                if (instance == null)
                    instance = FindObjectOfType<GameController>();

                return instance;
            }
        }

        void Start()
        {
            foreach (var a in GridSystem.Instance.Nodes.Take(GridSystem.Instance.GridColumns * 4))
            {
                var ball = ((GameObject)Instantiate(BallPrefabs[Random.Range(0, BallPrefabs.Length - 1)], a.transform.position, Quaternion.Euler(0, 0, 0))).GetComponent<BustAMoveBall>();
                ball.SetNode(a);
            }
        }

        public void GameOver()
        {
            Time.timeScale = 1;
            SceneManager.LoadScene("Scenes/BustAMove");
        }

        void Update()
        {
            var curAngle = PlayerPointer.rotation.eulerAngles.z;
            var spinDistance = Input.GetAxis("Horizontal");

            if (NextBallSpawnPoint.childCount == 0)
            {
                Instantiate(BallPrefabs[Random.Range(0, BallPrefabs.Length - 1)], NextBallSpawnPoint, false);
            }

            if (BallSpawnPoint.childCount == 0)
            {
                var nextBall = NextBallSpawnPoint.transform.GetChild(0);
                nextBall.GetComponent<Collider2D>().enabled = true;
                nextBall.parent = BallSpawnPoint;
                nextBall.transform.position = BallSpawnPoint.transform.position;
                var ball = (GameObject)Instantiate(BallPrefabs[Random.Range(0, BallPrefabs.Length - 1)], NextBallSpawnPoint, false);
                ball.GetComponent<Collider2D>().enabled = false;
                CanFire = true;
            }

            if (((curAngle <= 265) || spinDistance > 0) && ((curAngle >= 95) || spinDistance < 0))
            {
                LargeSprocket.Rotate(new Vector3(0, 0, -(spinDistance / 2)));
                MediumSprocket.Rotate(new Vector3(0, 0, spinDistance));
                SmallSprocket.Rotate(new Vector3(0, 0, -(spinDistance * 2)));
                PlayerPointer.Rotate(new Vector3(0, 0, -(spinDistance * 2)));
                Lever.Rotate(new Vector3(0, 0, spinDistance * 8f));
            }

            if (Input.GetButtonDown("Fire1") && CanFire)
            {
                var dir = Quaternion.AngleAxis(curAngle - 90, Vector3.forward) * Vector3.right;
                var child = BallSpawnPoint.GetChild(0);
                child.GetComponent<Rigidbody2D>().AddForce(dir * BallSpeed);
                CanFire = false;
            }
        }

        void FixedUpdate()
        {

        }

        void LateUpdate()
        {

        }
    }
}
