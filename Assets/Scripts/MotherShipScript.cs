﻿using UnityEngine;
using System.Collections;

public class MotherShipScript : MonoBehaviour
{
    public GameObject ScoreDisplay;
    public int Score;
    public float Speed;
    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.name.Contains("Enemy"))
        {
            Physics2D.IgnoreCollision(col.collider, GetComponent<Collider2D>());
        }
        else if (col.gameObject.name.Contains("Shot"))
        {
            col.gameObject.GetComponent<ShotLogic>().Explode();
            Destroy(col.gameObject, 0.03f);

            var display = (GameObject)Instantiate(ScoreDisplay, GameObject.Find("Canvas").transform);
            display.transform.position = Camera.main.WorldToScreenPoint(transform.position);
            display.SendMessage("DisplayScore", Score);

            Destroy(this.gameObject);
        }
    }

    void LateUpdate()
    {
        float movex = Speed * Time.deltaTime;        
        transform.Translate(movex, 0, 0);

        Vector3 viewPos = Camera.main.WorldToViewportPoint(this.transform.position);
        if (viewPos.x > 1)
        {
            Destroy(this.gameObject);
        }
    }
}
