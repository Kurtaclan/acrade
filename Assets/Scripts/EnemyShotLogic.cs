﻿using UnityEngine;
using System.Collections;

public class EnemyShotLogic : MonoBehaviour
{
    public float speed;
    public Sprite explosion;
    void FixedUpdate()
    {
        float movey = speed;
        movey *= Time.deltaTime;
        transform.Translate(0, movey, 0);

        Vector3 viewPos = Camera.main.WorldToViewportPoint(this.transform.position);
        if (viewPos.y < 0)
        {
            Destroy(this.gameObject);
        }
    }

    public void Explode()
    {
        this.GetComponent<SpriteRenderer>().sprite = explosion;
    }
}